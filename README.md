##Readme
This repository contains the two projects that were extensifly looked at in the masterthesis "Lossy Image Compression with Neural Networks". 
The "compression" folder contains the HiFiC project with all the changes and experiments descibed in the thesis (original: https://github.com/tensorflow/compression/tree/master/models/hific) 
The "Perceptual Lossy Commpression" folder contains the project of Yan et al with all the changes and experiments despribed in the thesis (original: https://github.com/ZeyuYan/Perceptual-Lossy-Compression)
