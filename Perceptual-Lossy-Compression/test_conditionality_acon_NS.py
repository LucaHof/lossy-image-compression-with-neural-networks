import os, time
import matplotlib.pyplot as plt
import itertools
import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from torch.autograd import Variable
from networks2 import generator1, generator2, discriminator
import numpy as np
from random import randrange

def satollo_shuffle(items) -> None:
    """Sattolo's algorithm."""
    i = len(items)
    while i > 1:
        i = i - 1
        j = randrange(i)  # 0 <= j <= i-1
        items[j], items[i] = items[i], items[j]

        
def make_hist(data_nor, data_con, path):
    n_bins = 40
    
    fig,axs = plt.subplots(1,2)
    fig.set_size_inches((10,5))
    axs[0].hist(data_nor[0],n_bins, alpha = 0.5, color= 'b', label= 'generated conditional')
    axs[0].hist(data_nor[1],n_bins, alpha = 0.5, color= 'y', label= 'real conditional')
    axs[0].hist(data_nor[2],n_bins, alpha = 0.5, color= 'g', label= 'generated a-contrario')
    axs[0].hist(data_nor[3],n_bins, alpha = 0.5, color= 'r', label= 'real a-contraio')
    axs[0].legend()
    
    #axs[1].hist(data_con[0],n_bins, alpha = 0.5, color= 'b')
    #axs[1].hist(data_con[1],n_bins, alpha = 0.5, color= 'y')
    #axs[1].hist(data_con[2],n_bins, alpha = 0.5, color= 'g')
    #axs[1].hist(data_con[3],n_bins, alpha = 0.5, color= 'r')
    fig.text(0.5, 0.04, "condionality", ha='center')
    plt.savefig(path+'conditionality.png')
    
    plt.close()

def compute_cond_values(rate, G1, G2, D):
    
    transform = transforms.Compose([
            transforms.Resize(img_size),
            transforms.ToTensor()
    ])
    
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('dataset', train=False, download=True, transform=transform),
        batch_size=batch_size, shuffle=False)
    
    gen_cond = []
    real_cond = []
    gen_acon = []
    real_acon = []

    for x_, y_ in train_loader:
        mini_batch = x_.size()[0]

        x_ = Variable(x_.cuda())
        x_mse_, bitstream = G1(x_)

        z_ = torch.randn((mini_batch, 100 - rate)).view(-1, 100 - rate, 1, 1)
        z_ = Variable(z_.cuda())
        G2_input = torch.cat([bitstream.data, z_], 1)

        G_results = G2(G2_input)

        D_result_gen_cond = D(G_results,bitstream.data)
        D_result_gen_cond = D_result_gen_cond.cpu().detach().numpy()
        D_result_gen_cond = D_result_gen_cond.reshape(D_result_gen_cond.size)
        gen_cond = gen_cond + D_result_gen_cond.tolist()

        D_result_real_cond = D(x_, bitstream.data)
        D_result_real_cond = D_result_real_cond.cpu().detach().numpy()
        D_result_real_cond = D_result_real_cond.reshape(D_result_real_cond.size)
        real_cond = real_cond + D_result_real_cond.tolist()
        
        arr = [i for i in range(0, mini_batch)]
        satollo_shuffle(arr)
        bit_f = bitstream[arr,:,:,:].data

        D_result_gen_acon = D(G_results, bit_f.data)
        D_result_gen_acon = D_result_gen_acon.cpu().detach().numpy()
        D_result_gen_acon = D_result_gen_acon.reshape(D_result_gen_acon.size)
        gen_acon = gen_acon + D_result_gen_acon.tolist()        

        D_result_real_acon = D(x_, bit_f.data)
        D_result_real_acon = D_result_real_acon.cpu().detach().numpy()
        D_result_real_acon = D_result_real_acon.reshape(D_result_real_acon.size)
        real_acon = real_acon + D_result_real_acon.tolist()
    
    data = []
    #print(gen_cond)
    #print(real_cond)
    #print(gen_acon)
    #print(real_acon)
    data.append(gen_cond)
    data.append(real_cond)
    data.append(gen_acon)
    data.append(real_acon)
    
    return data

def train_disc(G1,G2,D):
    # training parameters
    batch_size = 128*2
    lr = 0.001
    train_epoch = 100
    pretrain_epoch = 1
    lambda_gp = 10
    pretrained = False
    img_size = 32
    
    # data_loader
    transform = transforms.Compose([
            transforms.Resize(img_size),
            transforms.ToTensor()
    ])
    
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST('dataset', train=True, download=True, transform=transform),
        batch_size=batch_size, shuffle=True, drop_last=True)

    D_optimizer = optim.RMSprop(D.parameters(), lr=lr)

    print('Discriminator converging started')

    for x_, y_ in train_loader:
        x_split = torch.tensor_split(x_,2)
        con_ = x_split[0]
        acon_ = x_split[1]
        
        mini_batch = con_.size()[0]
        
        con_ = Variable(con_.cuda())
        con_mse_, con_bitstream = G1(con_)

        acon_ = Variable(acon_.cuda())
        acon_mse_, acon_bitstream = G1(acon_)

        # train discriminator D

        D.zero_grad()

        D_result = D(con_, con_bitstream.data).squeeze()
        D_real_loss= torch.log(torch.sigmoid(D_result)).mean()

        z_ = torch.randn((mini_batch, 100 - rate)).view(-1, 100 - rate, 1, 1)
        z_ = Variable(z_.cuda())
        G2_input = torch.cat([con_bitstream.data, z_], 1)

        G_result = G2(G2_input)
        D_result = D(G_result.data, con_bitstream.data).squeeze()

        D_fake_loss = torch.log(1 - torch.sigmoid(D_result)).mean()
        D_fake_score = D_result.data.mean()
        
        arr = [i for i in range(0, mini_batch)]
        satollo_shuffle(arr)
        acon_bitstream_shuffled = acon_bitstream[arr,:,:,:].data
        
        D_result_acon = D(acon_, acon_bitstream_shuffled.data).squeeze()
        D_real_loss_acon = torch.log(1 - torch.sigmoid(D_result_acon)).mean()
        
        z_ = torch.randn((mini_batch, 100 - rate)).view(-1, 100 - rate, 1, 1)
        z_ = Variable(z_.cuda())
        G2_input_acon = torch.cat([acon_bitstream.data, z_], 1)

        G_result_acon = G2(G2_input_acon)
        D_result_acon = D(G_result_acon, acon_bitstream_shuffled.data).squeeze()

        D_fake_loss_acon = torch.log(1 - torch.sigmoid(D_result_acon)).mean()

        D_train_loss = -(D_real_loss + D_fake_loss + D_real_loss_acon + D_fake_loss_acon)
        
        D_train_loss.backward()
        D_optimizer.step()
        
def eval_network(root, model):
    # data_loader
    # network
    G1 = generator1(64, rate)
    G2 = generator2(32)
    D = discriminator(16, rate)
    G1.load_state_dict(torch.load(root + model + 'generator1_acon_pro_param.pkl'))
    G2.load_state_dict(torch.load(root + model + 'generator2_acon_pro_param.pkl'))
    D.load_state_dict(torch.load(root + model + 'discriminator_acon_pro_param.pkl'))
    G1.cuda()
    G2.cuda()
    D.cuda()

    G1.train(False)
    G2.train(False)
    D.train(True)

    fixed_p = root + 'samples/pro_' + model

    data_noconverge = compute_cond_values(rate, G1, G2, D)

    train_disc(G1, G2, D)

    data_converge = compute_cond_values(rate, G1, G2, D)

    make_hist(data_noconverge, data_converge, path=fixed_p)
        
# testing parameters
batch_size = 128
rate = 30
img_size = 32

# results save folder
root = 'result/'
if not os.path.isdir(root):
    os.mkdir(root)
if not os.path.isdir(root + 'samples'):
    os.mkdir(root + 'samples')

print("evaluate network after pretraining")    
#eval_network(root,'MNIST_pretrain_')
print("evaluate network after halfway point")  
#eval_network(root, 'MNIST_half_')
print("evaluate trained network") 
eval_network(root,'MNIST_')
print("finished evaluating")

