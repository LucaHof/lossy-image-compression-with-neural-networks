import os, time
import matplotlib.pyplot as plt
import itertools
import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from torchvision import datasets, transforms
from torch.autograd import Variable
from networks import generator1, generator2, discriminator

# training parameters
batch_size = 128
lr = 0.001
train_epoch = 100
pretrain_epoch = 1
lambda_gp = 10
pretrained = False
rate = 30
img_size = 32

# results save folder
root = 'result/'
model = 'MNIST_'
if not os.path.isdir(root):
    os.mkdir(root)
    
# data_loader
transform = transforms.Compose([
        transforms.Resize(img_size),
        transforms.ToTensor()
])
train_loader = torch.utils.data.DataLoader(
    datasets.MNIST('dataset', train=True, download=True, transform=transform),
    batch_size=batch_size, shuffle=True)

# network
G1 = generator1(64, rate)
G2 = generator2(32)
D = discriminator(16, rate)

G1.weight_init(mean=0.0, std=0.02)
G2.weight_init(mean=0.0, std=0.02)
D.weight_init(mean=0.0, std=0.02)
    
G1.cuda()
G2.cuda()
D.cuda()

G1_optimizer = optim.RMSprop(G1.parameters(), lr=lr/10)
G2_optimizer = optim.RMSprop(G2.parameters(), lr=lr/10)
D_optimizer = optim.RMSprop(D.parameters(), lr=lr)

train_hist = {}
train_hist['D_losses'] = []
train_hist['G_losses'] = []
train_hist['G1_mse'] = []
train_hist['G2_mse'] = []
train_hist['2G1_mse'] = []
train_hist['per_epoch_ptimes'] = []
train_hist['total_ptime'] = []
train_hist['D_real_con_loss'] = []
train_hist['D_fake_con_loss'] = []

print('training start!')
start_time = time.time()
for epoch in range(train_epoch):
    D_losses = []
    G_losses = []
    G1_mse = []
    G1_2_mse = []
    G2_mse = []
    D_t_loss = []
    D_real_con_losses = []
    D_fake_con_losses = []

    # learning rate decay
    if (epoch+1) == 50:
        G1_optimizer.param_groups[0]['lr'] /= 10
        print("learning rate change!")

    epoch_start_time = time.time()
    for x_, y_ in train_loader:
        # train generator1 G1
        G1.zero_grad()
        mini_batch = x_.size()[0]
        
        x_ = Variable(x_.cuda())
        x_mse_, bitstream = G1(x_)
        G1_loss = torch.mean((x_mse_ - x_)**2)

        G1_loss.backward()
        G1_optimizer.step()

        G1_mse.append(G1_loss.data)
        G1_2_mse.append(2*G1_loss.data)

    epoch_end_time = time.time()
    per_epoch_ptime = epoch_end_time - epoch_start_time

    print('[%d/%d] - ptime: %.2f, G1_mse: %.4f, G2_mse: %.4f' % ((epoch + 1), train_epoch, per_epoch_ptime, torch.mean(torch.FloatTensor(G1_mse)), torch.mean(torch.FloatTensor(G2_mse))))
    
    fixed_p = root + 'Fixed_results/' + model + str(epoch + 1) + '.png'
    train_hist['G1_mse'].append(torch.mean(torch.FloatTensor(G1_mse)))
    train_hist['2G1_mse'].append(torch.mean(torch.FloatTensor(G1_2_mse)))
    train_hist['per_epoch_ptimes'].append(per_epoch_ptime)

end_time = time.time()
total_ptime = end_time - start_time
train_hist['total_ptime'].append(total_ptime)

print("Avg one epoch ptime: %.2f, total %d epochs ptime: %.2f" % (torch.mean(torch.FloatTensor(train_hist['per_epoch_ptimes'])), train_epoch, total_ptime))
print("Training finish!... save training results")
torch.save(G1.state_dict(), root + model + 'generator1_MSE_param.pkl')
torch.save(G2.state_dict(), root + model + 'generator2_MSE_param.pkl')
torch.save(D.state_dict(), root + model + 'discriminator_MSE_param.pkl')
with open(root + model + 'train_MSE_hist.pkl', 'wb') as f:
    pickle.dump(train_hist, f)

np.savetxt(root+model+'G1_mse.txt', train_hist['G1_mse'], delimiter=" ")

