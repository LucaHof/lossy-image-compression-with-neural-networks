import os, time
import matplotlib.pyplot as plt
import itertools
import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from torchvision import datasets, transforms
from torch.autograd import Variable
from networks import generator1, generator2, discriminator
from random import randrange

def satollo_shuffle(items) -> None:
    """Sattolo's algorithm."""
    i = len(items)
    while i > 1:
        i = i - 1
        j = randrange(i)  # 0 <= j <= i-1
        items[j], items[i] = items[i], items[j]

def show_train_hist(hist, show = False, save = False, path = 'Train_hist.png'):
    x = range(len(hist['D_losses']))

    y1 = hist['D_losses']
    y2 = hist['G_losses']

    plt.plot(x, y1, label='D_loss')
    plt.plot(x, y2, label='G_loss')

    plt.xlabel('Epoch')
    plt.ylabel('Loss')

    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()

def show_train_dlosses(hist, show = False, save = False, path = 'Train_dlosses.png'):
    x = range(len(hist['D_losses']))

    y1 = hist['D_real_con_loss']
    y2 = hist['D_fake_con_loss']
    y3 = hist['D_real_acon_loss']
    y4 = hist['D_fake_acon_loss']

    plt.plot(x, y1, label='D_real_con_loss')
    plt.plot(x, y2, label='D_fake_con_loss')
    plt.plot(x, y3, label='D_real_acon_loss')
    plt.plot(x, y4, label='D_fake_acon_loss')

    plt.xlabel('Epoch')
    plt.ylabel('Loss')

    plt.legend(loc=4)
    plt.grid(True)
    plt.tight_layout()

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()

def show_train_mse(hist, show = False, save = False, path = 'Train_mse.png'):
    x = range(len(hist['G1_mse']))

    y1 = hist['G1_mse']
    y2 = hist['G2_mse']
    y3 = hist['2G1_mse']

    plt.plot(x, y1, label='G1_mse')
    plt.plot(x, y2, label='G2_mse')
    plt.plot(x, y3, label='2*G1_mse')

    plt.xlabel('Epoch')
    plt.ylabel('MSE')

    plt.legend(loc='best')
    plt.grid(True)
    plt.tight_layout()

    if save:
        plt.savefig(path)

    if show:
        plt.show()
    else:
        plt.close()

# training parameters
# --- changed by Nikolai10 start ---
#batch_size = 128
batch_size = 128*2 
# --- changed by Nikolai10 end ---
lr = 0.001
train_epoch = 1
lambda_gp = 10
lambda_acon = 0.001
pretrained = False
rate = 30
img_size = 32

# results save folder
root = 'result/'
model = 'MNIST_'
if not os.path.isdir(root):
    os.mkdir(root)
    
# data_loader
transform = transforms.Compose([
        transforms.Resize(img_size),
        transforms.ToTensor()
])
# --- changed by Nikolai10 start ---
# added drop_last=True (sorgt dafür, dass alle mini-batches die gleiche Anzahl an Samples hat)
train_loader = torch.utils.data.DataLoader(
    datasets.MNIST('dataset', train=True, download=True, transform=transform),
    batch_size=batch_size, shuffle=True, drop_last=True)
# --- changed by Nikolai10 end ---

# network
G1 = generator1(64, rate)
G2 = generator2(32)
D = discriminator(16, rate)

G1.load_state_dict(torch.load(root + model + 'generator1_MSE_param.pkl'))
G2.load_state_dict(torch.load(root + model + 'generator2_MSE_param.pkl'))
D.load_state_dict(torch.load(root + model + 'discriminator_MSE_param.pkl'))

G1.cuda()
G2.cuda()
D.cuda()

G1_optimizer = optim.RMSprop(G1.parameters(), lr=lr/10)
G2_optimizer = optim.RMSprop(G2.parameters(), lr=lr/10)
D_optimizer = optim.RMSprop(D.parameters(), lr=lr)

train_hist = {}
train_hist['D_losses'] = []
train_hist['G_losses'] = []
train_hist['G1_mse'] = []
train_hist['G2_mse'] = []
train_hist['2G1_mse'] = []
train_hist['per_epoch_ptimes'] = []
train_hist['total_ptime'] = []
train_hist['D_real_con_loss'] = []
train_hist['D_fake_con_loss'] = []
train_hist['D_real_acon_loss'] = []
train_hist['D_fake_acon_loss'] = []

print('training start!')
start_time = time.time()
for epoch in range(train_epoch):
    D_losses = []
    G_losses = []
    G1_mse = []
    G1_2_mse = []
    G2_mse = []
    D_real_con_losses = []
    D_fake_con_losses = []
    D_real_acon_losses = []
    D_fake_acon_losses = []
        
    if epoch == 50:
        G2_optimizer.param_groups[0]['lr'] /= 10
        D_optimizer.param_groups[0]['lr'] /= 10
        print("learning rate change! saving model")
        torch.save(G1.state_dict(), root + model + 'half_generator1_param.pkl')
        torch.save(G2.state_dict(), root + model + 'half_generator2_param.pkl')
        torch.save(D.state_dict(), root + model + 'half_discriminator_param.pkl')

    epoch_start_time = time.time()
    for x_, y_ in train_loader:
        # --- changed by Nikolai10 start ---
        # tensor_split in meiner PyTorch Version nicht vorhanden (ignorieren...)
        x_split = torch.tensor_split(x_,2)
        con_ = x_split[0]
        acon_ = x_split[1]
        #con_ = x_[0:128]
        #acon_ = x_[128:]
        # --- changed by Nikolai10 end ---
        # train generator1 G1
        G1.zero_grad()
        mini_batch = con_.size()[0]
        
        con_ = Variable(con_.cuda())
        con_mse_, con_bitstream = G1(con_)

        acon_ = Variable(acon_.cuda())
        acon_mse_, acon_bitstream = G1(acon_)
        
        G1_loss = torch.mean((con_mse_ - con_)**2)

        G1_mse.append(G1_loss.data)
        G1_2_mse.append(2*G1_loss.data)
        
        # train discriminator D
        D.zero_grad()

        D_result = D(con_, con_bitstream.data).squeeze()
        D_real_loss= torch.log(torch.sigmoid(D_result)).mean()
        D_real_con_losses.append(D_real_loss)
        

        z_ = torch.randn((mini_batch, 100 - rate)).view(-1, 100 - rate, 1, 1)
        z_ = Variable(z_.cuda())
        G2_input = torch.cat([con_bitstream.data, z_], 1)

        G_result = G2(G2_input)
        D_result = D(G_result.data, con_bitstream.data).squeeze()

        D_fake_loss = torch.log(1 - torch.sigmoid(D_result)).mean()
        D_fake_con_losses.append(D_fake_loss)
        D_fake_score = D_result.data.mean()
        
        arr = [i for i in range(0, mini_batch)]
        satollo_shuffle(arr)
        acon_bitstream_shuffled = acon_bitstream[arr,:,:,:].data
        #acon_shuffled = acon_[arr,:,:,:].data
        
        D_result_acon = D(acon_, acon_bitstream_shuffled.data).squeeze()
        D_real_loss_acon = torch.log(1 - torch.sigmoid(D_result_acon)).mean()
        D_real_acon_losses.append(D_real_loss_acon)
        
        z_ = torch.randn((mini_batch, 100 - rate)).view(-1, 100 - rate, 1, 1)
        z_ = Variable(z_.cuda())
        G2_input = torch.cat([acon_bitstream.data, z_], 1)

        G_result_acon = G2(G2_input)
        # --- changed by Nikolai10 start ---
        # @Lucy: acon_bitstream identisch sowohl zur Generierung als auch Diskriminierung
        # -> shuffle der generierten Bilder, identischer acon_bitstream
        # D_result_acon = D(G_result_acon.data, acon_bitstream.data).squeeze()
        #G_result_acon_shuffled = G_result_acon[arr,:,:,:].data
        D_result_acon = D(G_result_acon, acon_bitstream_shuffled.data).squeeze()
        # --- changed by Nikolai10 end ---

        D_fake_loss_acon = torch.log(1- torch.sigmoid(D_result_acon)).mean()
        D_fake_acon_losses.append(D_fake_loss_acon)
        

        # --- changed by Nikolai10 start ---
        #D_train_loss = D_real_loss + D_fake_loss + D_real_loss_acon + D_fake_loss_acon
        # Skalierung der "fake" batches -> offensichtlich hier notwendig (entscheidender Faktor)
        # vermutlich aufgrund des Wertebereichs von WGAN-GP (gerne weiter analysieren...)
        D_train_loss = - (D_real_loss + D_fake_loss + D_real_loss_acon + D_fake_loss_acon)
        #D_train_loss = D_real_loss + ((1 - lambda_acon) * D_fake_loss) + (lambda_acon * (D_real_loss_acon + D_fake_loss_acon))
        # --- changed by Nikolai10 end ---
        D_train_loss.backward()
        D_optimizer.step()

        D_losses.append(D_train_loss.data)
        '''
        #gradient penalty
        D.zero_grad()
        alpha = torch.rand(con_.size(0), 1, 1, 1)
        alpha1 = alpha.cuda().expand_as(con_.data)
        alpha2 = alpha.cuda().expand_as(con_bitstream)
        interpolated1 = Variable(alpha1 * con_.data + (1 - alpha1) * G_result.data, requires_grad=True)
        interpolated2 = Variable(alpha2 * con_bitstream.data + (1 - alpha2) * acon_bitstream_shuffled.data, requires_grad=True)

        out = D(interpolated1, interpolated2).squeeze()

        grad = torch.autograd.grad(outputs=out,
                                   inputs=[interpolated1, interpolated2],
                                   grad_outputs=torch.ones(out.size()).cuda(),
                                   retain_graph=True,
                                   create_graph=True,
                                   only_inputs=True)[0]

        grad = grad.view(grad.size(0), -1)
        grad_l2norm = torch.sqrt(torch.sum(grad ** 2, dim=1))
        d_loss_gp = torch.mean((grad_l2norm - 1) ** 2)

        # Backward + Optimize
        gp_loss = lambda_gp * d_loss_gp

        gp_loss.backward()
        D_optimizer.step()
        '''
        # train generator G2
        D.zero_grad()
        G2.zero_grad()

        z_ = torch.randn((mini_batch, 100 - rate)).view(-1, 100 - rate, 1, 1)
        z_ = Variable(z_.cuda())
        G2_input = torch.cat([con_bitstream.data, z_], 1)

        G_result = G2(G2_input)
        D_result = D(G_result, con_bitstream.data).squeeze()

        G_train_loss = - torch.log(torch.sigmoid(D_result)).mean()

        G_train_loss.backward()
        G2_optimizer.step()

        G_losses.append(G_train_loss.data)
        g2_mse = torch.mean((G_result.data - con_)**2)
        G2_mse.append(g2_mse.data)


    epoch_end_time = time.time()
    per_epoch_ptime = epoch_end_time - epoch_start_time

    # --- changed by Nikolai10 start ---
    # TODO @Lucy: bitte loss Werte (D_real_loss, D_fake_loss, D_real_loss_acon, D_fake_loss_acon)
    # individuell tracken und analysieren... 
    # in der klassichen "VanillaGAN" Notation müssten D_real_loss_acon, D_fake_loss_acon irgendwann gegen 0 konvergieren
    # (hier sind die Werte anders); andernfalls länger trainieren
    # --- changed by Nikolai10 end ---
    print('[%d/%d] - ptime: %.2f, loss_d: %.3f, G1_mse: %.4f, G2_mse: %.4f' % ((epoch + 1), train_epoch, per_epoch_ptime, torch.mean(torch.FloatTensor(D_losses)), torch.mean(torch.FloatTensor(G1_mse)), torch.mean(torch.FloatTensor(G2_mse))))
    
    print('loss_real_con %.3f, loss_fake_con %.3f, loss_real_acon %.3f, loss_fake_acon %.3f,' % (torch.mean(torch.FloatTensor(D_real_con_losses)), torch.mean(torch.FloatTensor(D_fake_con_losses)), torch.mean(torch.FloatTensor(D_real_acon_losses)), torch.mean(torch.FloatTensor(D_fake_acon_losses))))
    
    fixed_p = root + 'Fixed_results/' + model + str(epoch + 1) + '.png'
    train_hist['D_losses'].append(torch.mean(torch.FloatTensor(D_losses)))
    train_hist['G_losses'].append(torch.mean(torch.FloatTensor(G_losses)))
    train_hist['G1_mse'].append(torch.mean(torch.FloatTensor(G1_mse)))
    train_hist['2G1_mse'].append(torch.mean(torch.FloatTensor(G1_2_mse)))
    train_hist['G2_mse'].append(torch.mean(torch.FloatTensor(G2_mse)))
    train_hist['D_real_con_loss'].append(torch.mean(torch.FloatTensor(D_real_con_losses)))
    train_hist['D_fake_con_loss'].append(torch.mean(torch.FloatTensor(D_fake_con_losses)))
    train_hist['D_real_acon_loss'].append(torch.mean(torch.FloatTensor(D_real_acon_losses)))
    train_hist['D_fake_acon_loss'].append(torch.mean(torch.FloatTensor(D_fake_acon_losses)))
    train_hist['per_epoch_ptimes'].append(per_epoch_ptime)

end_time = time.time()
total_ptime = end_time - start_time
train_hist['total_ptime'].append(total_ptime)

print("Avg one epoch ptime: %.2f, total %d epochs ptime: %.2f" % (torch.mean(torch.FloatTensor(train_hist['per_epoch_ptimes'])), train_epoch, total_ptime))
print("Training finish!... save training results")
torch.save(G1.state_dict(), root + model + 'generator1_acon_param.pkl')
torch.save(G2.state_dict(), root + model + 'generator2_acon_param.pkl')
torch.save(D.state_dict(), root + model + 'discriminator_acon_param.pkl')
with open(root + model + 'train_acon_hist.pkl', 'wb') as f:
    pickle.dump(train_hist, f)

show_train_hist(train_hist, save=True, path=root + model + 'train_hist_acon_NS.png')
show_train_mse(train_hist, save=True, path=root + model + 'train_mse_acon_NS.png')
show_train_dlosses(train_hist, save=True, path=root + model + 'train_dlosses_acon_NS.png')
np.savetxt(root+model+'G1_mse.txt', train_hist['G1_mse'], delimiter=" ")
np.savetxt(root+model+'G2_mse.txt', train_hist['G2_mse'], delimiter=" ")
