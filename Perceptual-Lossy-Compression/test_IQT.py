import os, time
import matplotlib.pyplot as plt
import itertools
import pickle
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets
from torchvision import transforms as T
from torch.autograd import Variable
from networks2 import generator1, generator2, discriminator
from PIL import Image
from os import system

def show_result(img, counter, show = False, save = False, path = 'result/IQT_temp/GANImages/'):

    G1.eval()
    G2.eval()
    img_mse, bitstream = G1(img)

    z_ = torch.randn((100, 100-rate)).view(-1, 100-rate, 1, 1)
    z_ = Variable(z_.cuda())
    z_ = torch.cat([bitstream.data, z_], 1)
    img_p = G2(z_)

    size_figure_grid = 10

    fig, ax = plt.subplots(size_figure_grid, size_figure_grid, figsize=(5, 5))
    for i, j in itertools.product(range(size_figure_grid), range(size_figure_grid)):
        ax[i, j].get_xaxis().set_visible(False)
        ax[i, j].get_yaxis().set_visible(False)
    for k in range(10*10):
        i = k // 10
        j = k % 10
        ax[i, j].cla()
        ax[i, j].imshow(img[k, 0].cpu().data.numpy(), cmap='gray')

    label = 'input'
    fig.text(0.5, 0.04, '', ha='center')
    plt.savefig(path + 'originalImages/' + str(counter) +'.png')

    fig, ax = plt.subplots(size_figure_grid, size_figure_grid, figsize=(5, 5))
    for i, j in itertools.product(range(size_figure_grid), range(size_figure_grid)):
        ax[i, j].get_xaxis().set_visible(False)
        ax[i, j].get_yaxis().set_visible(False)
    for k in range(10*10):
        i = k // 10
        j = k % 10
        ax[i, j].cla()
        ax[i, j].imshow(img_p[k, 0].cpu().data.numpy(), cmap='gray')

    label = 'P=0'
    fig.text(0.5, 0.04, '', ha='center')
    plt.savefig(path+ str(counter) + '.png')
    
    plt.cla()
    plt.close()


# testing parameters
batch_size = 100
rate = 10
img_size = 32

# results save folder
root = 'result/'
og_path = 'FID_temp/originalImages/'
gen_path = 'FID_temp/MSEImages/'
gen2_path = 'FID_temp/GANImages/'
model = 'MNIST_'
if not os.path.isdir(root):
    os.mkdir(root)
if not os.path.isdir(root + 'samples'):
    os.mkdir(root + 'samples')
    
# data_loader
transform = T.Compose([
        T.Resize(img_size),
        T.ToTensor()
])
train_loader = torch.utils.data.DataLoader(
    datasets.MNIST('dataset', train=False, download=True, transform=transform),
    batch_size=batch_size, shuffle=False,drop_last=True)

# network
G1 = generator1(64, rate)
G2 = generator2(32)
D = discriminator(16, rate)
G1.load_state_dict(torch.load(root + model + 'generator1_pro_param_10.pkl'))
G2.load_state_dict(torch.load(root + model + 'generator2_pro_param_10.pkl'))
D.load_state_dict(torch.load(root + model + 'discriminator_pro_param_10.pkl'))
G1.cuda()
G2.cuda()
D.cuda()

counter = 0

for x_, y_ in train_loader:
    mini_batch = x_.size()[0]
        
    x_ = Variable(x_.cuda())
    x_mse_, bitstream = G1(x_)

    z_ = torch.randn((mini_batch, 100 - rate)).view(-1, 100 - rate, 1, 1)
    z_ = Variable(z_.cuda())
    G2_input = torch.cat([bitstream.data, z_], 1)

    G_result = G2(G2_input)
    
    
    x_split = torch.tensor_split(x_,mini_batch)
    mse_split = torch.tensor_split(x_mse_,mini_batch)
    gan_split = torch.tensor_split(G_result,mini_batch)

    show_result(x_[0:100,:,:,:], counter, save=True)
    
    counter += 1


print("IQT GAN")
system("python IQT/test.py ")



