import argparse
import collections
import glob
import itertools
import os
import sys
import matplotlib.pyplot as plt

import numpy as np
from PIL import Image
import tensorflow.compat.v1 as tf
import tensorflow_compression as tfc

from hific import configs
from hific import helpers
#change this import to evalute diffent models only models with acontrario option work here
from hific import model_acon as model

import numpy as np
from random import randrange

tf.logging.set_verbosity(tf.logging.INFO)

Nodes = collections.namedtuple(
    "Nodes",                    # Expected ranges for RGB:
    ["input_image",             # [0, 255]
     "input_image_scaled",      # [0, 1]
     "reconstruction",          # [0, 255]
     "reconstruction_scaled",   # [0, 1]
     "latent_quantized"])       # Latent post-quantization.

def satollo_shuffle(items) -> None:
    """Sattolo's algorithm."""
    i = len(items)
    while i > 1:
        i = i - 1
        j = randrange(i)  # 0 <= j <= i-1
        items[j], items[i] = items[i], items[j]
        
# Show custom tf.logging calls.
tf.logging.set_verbosity(tf.logging.INFO)


def make_hist(data_nor, data_con, path):
    n_bins = 40
    
    fig,axs = plt.subplots(1,2)
    fig.set_size_inches((10,5))
    axs[0].hist(data_nor[0],n_bins, alpha = 0.5, color= 'b', label= 'generated conditional')
    axs[0].hist(data_nor[1],n_bins, alpha = 0.5, color= 'y', label= 'real conditional')
    axs[0].hist(data_nor[2],n_bins, alpha = 0.5, color= 'g', label= 'generated a-contrario')
    axs[0].hist(data_nor[3],n_bins, alpha = 0.5, color= 'r', label= 'real a-contraio')
    axs[0].legend()
    
    #axs[1].hist(data_con[0],n_bins, alpha = 0.5, color= 'b')
    #axs[1].hist(data_con[1],n_bins, alpha = 0.5, color= 'y')
    #axs[1].hist(data_con[2],n_bins, alpha = 0.5, color= 'g')
    #axs[1].hist(data_con[3],n_bins, alpha = 0.5, color= 'r')
    fig.text(0.5, 0.04, "condionality", ha='center')
    plt.savefig(path+'conditionality.png')
    
    plt.close()

def eval_trained_model(config_name,
                       ckpt_dir,
                       out_dir,
                       tfds_arguments: helpers.TFDSArguments,
                       lpips_weight_path,
                       max_images=1000):
    """Evaluate a trained model."""
    config = configs.get_config(config_name)
    hific = model.HiFiC(config, helpers.ModelMode.CONDITIONALITY)

    # Note: Automatically uses the validation split for TFDS.
    dataset = hific.build_input(
        batch_size=128,
        crop_size=256,
        tfds_arguments=tfds_arguments)
    
    iterator = tf.data.make_one_shot_iterator(dataset)
    
    get_next_image = iterator.get_next()
    input_image = get_next_image['input_image']
    output_image, bitstring, gennodes = hific.build_model(**get_next_image)
    dis_out = hific._compute_discriminator_out(gennodes,False)

    input_image = tf.cast(tf.round(input_image[0, ...]), tf.uint8)
    output_image = tf.cast(tf.round(output_image[0, ...]), tf.uint8)
    
    os.makedirs(out_dir, exist_ok=True)
    
    
    accumulated_metrics = collections.defaultdict(list)
    gen_cond = []
    real_cond = []
    gen_acon = []
    real_acon = []
    
    with tf.Session() as sess:
        hific.restore_trained_model(sess, ckpt_dir)
        #hific.prepare_for_arithmetic_coding(sess)
        
        print("sample non converged discriminator")
        for i in itertools.count(0):
            if max_images and i == max_images:
                break
            try:
                print(f'\niteration: {i}')
                inp_np, otp_np, bitstring_np, dis_out_np= sess.run([input_image, output_image, bitstring, dis_out])
                h, w, c = inp_np.shape
                assert c == 3
                d_real = tf.math.reduce_mean(dis_out_np.d_real_con).eval()
                real_cond.append(d_real)

                d_fake = tf.math.reduce_mean(dis_out_np.d_fake_con).eval()
                gen_cond.append(d_fake)
                
                d_acon_real = tf.math.reduce_mean(dis_out_np.d_real_acon).eval()
                real_acon.append(d_acon_real)

                d_acon_fake = tf.math.reduce_mean(dis_out_np.d_fake_acon).eval()
                gen_acon.append(d_acon_fake)
                
                
            except tf.errors.OutOfRangeError:
                print('No more inputs.')
                break
       
                
    data = []
    data.append(gen_cond)
    data.append(real_cond)
    data.append(gen_acon)
    data.append(real_acon)
    
    data_con = []
    
    return data, data_con

def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--config', required=True,
                        choices=configs.valid_configs(),
                        help='The config to use.')
    parser.add_argument('--ckpt_dir', required=True,
                        help=('Path to the folder where checkpoints of the '
                              'trained model are.'))
    parser.add_argument('--out_dir', required=True, help='Where to save outputs.')

    #parser.add_argument('--images_glob', help='If given, use TODO')

    parser.add_argument('--lpips_weight_path',
                        help=('Where to store the LPIPS weights. Defaults to '
                              'current directory'))
    helpers.add_tfds_arguments(parser)

    args = parser.parse_args(argv[1:])
    return args


def main(args):
   data_noconverge, data_con = eval_trained_model(args.config, args.ckpt_dir, args.out_dir, 
                           helpers.parse_tfds_arguments(args),
                           args.lpips_weight_path)
   
   make_hist(data_noconverge, data_con, args.out_dir)

if __name__ == '__main__':
    main(parse_args(sys.argv))