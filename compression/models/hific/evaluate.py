# Copyright 2020 Google LLC. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Eval models trained with train.py.

NOTE: To evaluate models used in the paper, use tfci.py! See README.md.
"""

import argparse
import collections
import glob
import itertools
import os
import sys

import numpy as np
from PIL import Image
import tensorflow.compat.v1 as tf
import tensorflow_compression as tfc

from hific import configs
from hific import helpers
from hific import model


def eval_trained_model(config_name,
                       ckpt_dir,
                       out_dir,
                       images_glob,
                       tfds_arguments: helpers.TFDSArguments,
                       lpips_weight_path,
                       max_images=None):
    """Evaluate a trained model."""
    config = configs.get_config(config_name)
    hific = model.HiFiC(config, helpers.ModelMode.EVALUATION)

    # Note: Automatically uses the validation split for TFDS.
    dataset = hific.build_input(
        batch_size=1,
        crop_size=None,
        images_glob=images_glob,
        tfds_arguments=tfds_arguments)
    image_names = get_image_names(images_glob)
    iterator = tf.data.make_one_shot_iterator(dataset)
    get_next_image = iterator.get_next()
    input_image = get_next_image['input_image']
    output_image, bitstring = hific.build_model(**get_next_image)

    input_image = tf.cast(tf.round(input_image[0, ...]), tf.uint8)
    output_image = tf.cast(tf.round(output_image[0, ...]), tf.uint8)

    # compute ms-ssim, lpips
    ms_ssim_score = get_ms_ssim(input_image, output_image)
    lpips_score = get_lpips(input_image, output_image, lpips_weight_path)

    os.makedirs(out_dir, exist_ok=True)

    accumulated_metrics = collections.defaultdict(list)

    out_dir_real = os.path.join(out_dir + 'patches_real/')
    out_dir_fake = os.path.join(out_dir + 'patches_fake/')
    if not os.path.exists(out_dir_real):
        os.makedirs(out_dir_real)
    if not os.path.exists(out_dir_fake):
        os.makedirs(out_dir_fake)

    with tf.Session() as sess:
        hific.restore_trained_model(sess, ckpt_dir)
        hific.prepare_for_arithmetic_coding(sess)

        for i in itertools.count(0):
            if max_images and i == max_images:
                break
            try:
                inp_np, otp_np, bitstring_np, ms_ssim_score_val, lpips_score_val = \
                    sess.run([input_image, output_image, bitstring, ms_ssim_score, lpips_score])

                h, w, c = inp_np.shape
                assert c == 3
                bpp = get_arithmetic_coding_bpp(
                    bitstring, bitstring_np, num_pixels=h * w)

                metrics = {'psnr': get_psnr(inp_np, otp_np),
                           'ms_ssim': ms_ssim_score_val,
                           'lpips': lpips_score_val,
                           'niqe': get_niqe(otp_np),
                           'bpp_real': bpp}

                # extract patches (FID, KID prep)
                otp_np_patches = sess.run(extract_patches(tf.expand_dims(otp_np, axis=0)))
                inp_np_patches = sess.run(extract_patches(tf.expand_dims(inp_np, axis=0)))

                metrics_str = ' / '.join(f'{metric}: {value:.5f}'
                                         for metric, value in metrics.items())
                print(f'Image {i: 4d}: {metrics_str}, saving in {out_dir}...')

                for metric, value in metrics.items():
                    accumulated_metrics[metric].append(value)

                # Save images.
                name = image_names.get(i, f'img_{i:010d}')
                Image.fromarray(inp_np).save(
                    os.path.join(out_dir, f'{name}_inp.png'))
                Image.fromarray(otp_np).save(
                    os.path.join(out_dir, f'{name}_otp_{bpp:.3f}.png'))

                # Save image patches.
                for i in range(otp_np_patches.shape[0]):
                    otp_np_patch_i = otp_np_patches[i]
                    inp_np_patch_i = inp_np_patches[i]
                    # save real...
                    Image.fromarray(inp_np_patch_i).save(os.path.join(out_dir_real, f'{name}_{i}.png'))
                    # save fake...
                    Image.fromarray(otp_np_patch_i).save(os.path.join(out_dir_fake, f'{name}_otp_{i}.png'))

            except tf.errors.OutOfRangeError:
                print('No more inputs.')
                break

    print('\n'.join(f'{metric}: {np.mean(values)}'
                    for metric, values in accumulated_metrics.items()))

    with open(os.path.join(out_dir, "results.txt"), "w") as text_file:
        text_file.write('\n'.join(f'{metric}: {np.mean(values)}'
                                  for metric, values in accumulated_metrics.items()))
    print('Done!')


def get_arithmetic_coding_bpp(bitstring, bitstring_np, num_pixels):
    """Calculate bitrate we obtain with arithmetic coding."""
    # TODO(fab-jul): Add `compress` and `decompress` methods.
    packed = tfc.PackedTensors()
    packed.pack(tensors=bitstring, arrays=bitstring_np)
    return len(packed.string) * 8 / num_pixels

def get_psnr(inp, otp):
    mse = np.mean(np.square(inp.astype(np.float32) - otp.astype(np.float32)))
    psnr = 20. * np.log10(255.) - 10. * np.log10(mse)
    return psnr

def get_ms_ssim(inp, otp):
    inp, otp = tf.expand_dims(inp, axis=0), tf.expand_dims(otp, axis=0)
    ms_ssim = tf.image.ssim_multiscale(inp, otp, max_val=255)[0]
    return ms_ssim

def get_lpips(inp, otp, lpips_weight_path):
    lpips_loss = model.LPIPSLoss(lpips_weight_path)
    inp, otp = tf.cast(inp, tf.float32), tf.cast(otp, tf.float32)
    inp, otp = tf.expand_dims(inp, axis=0), tf.expand_dims(otp, axis=0)
    reconstruction_scaled, input_image_scaled = otp/255., inp/255.
    lpips = lpips_loss(reconstruction_scaled, input_image_scaled)
    return lpips

def get_niqe(otp):
    otp_gray = utils.rgb2gray(otp)
    niqe = measure.niqe(otp_gray)[0]
    return niqe

def get_image_names(images_glob):
    if not images_glob:
        return {}
    return {i: os.path.splitext(os.path.basename(p))[0]
            for i, p in enumerate(sorted(glob.glob(images_glob)))}

def extract_patches(images, patch_size=256, stride=256):
    """
    Extract image patches as described in https://arxiv.org/pdf/2006.09965.pdf, Appendix A.7

    :param images:
    :param patch_size:
    :param stride:
    :return:
    """
    image_patches_orig = tf.image.extract_patches(
        images=images, sizes=[1, patch_size, patch_size, 1],
        strides=[1, stride, stride, 1],
        rates=[1, 1, 1, 1],
        padding='VALID')

    images_shifted = images[:, patch_size//2:, patch_size//2:, :]
    image_patches_shifted = tf.image.extract_patches(
        images=images_shifted, sizes=[1, patch_size, patch_size, 1],
        strides=[1, stride, stride, 1],
        rates=[1, 1, 1, 1],
        padding='VALID')

    # reshape to (N1, 256, 256, 3)
    n, h, w, c = image_patches_orig.shape
    n_p = int((n * h * w * c) // (patch_size * patch_size * 3))
    image_patches_orig = tf.reshape(image_patches_orig, (n_p, patch_size, patch_size, 3))
    # reshape to (N2, 256, 256, 3)
    n, h, w, c = image_patches_shifted.shape
    n_p = int((n * h * w * c) // (patch_size * patch_size * 3))
    image_patches_shifted = tf.reshape(image_patches_shifted, (n_p, patch_size, patch_size, 3))

    # concat to (N1+N2, 256, 256, 3)
    image_patches = tf.concat([image_patches_orig, image_patches_shifted], axis=0)
    return image_patches

def eval_trained_model_dist(out_dir_real, out_dir_fake, out_file):
    # compute FID, KID
    print('\ncomputing FID, KID...')
    fid_score = fid.compute_fid(out_dir_real, out_dir_fake)
    kid_score = fid.compute_kid(out_dir_real, out_dir_fake)
    print(f'\nFID-score: {fid_score}\nKID-score: {kid_score}')
    with open(out_file, "a") as fo:
        fo.write(f'\nFID-score: {fid_score}\nKID-score: {kid_score}')
    print('Done!')

def parse_args(argv):
    """Parses command line arguments."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--config', required=True,
                        choices=configs.valid_configs(),
                        help='The config to use.')
    parser.add_argument('--ckpt_dir', required=True,
                        help=('Path to the folder where checkpoints of the '
                              'trained model are.'))
    parser.add_argument('--out_dir', required=True, help='Where to save outputs.')

    parser.add_argument('--images_glob', help='If given, use TODO')

    parser.add_argument('--lpips_weight_path',
                        help=('Where to store the LPIPS weights. Defaults to '
                              'current directory'))
    parser.add_argument('--mode',
                        help=('0 : psnr+ms-ssim+niqe+lpips; 1 : fid+kid; always run mode 0 before mode 1 to extract image patches!'))
    helpers.add_tfds_arguments(parser)

    args = parser.parse_args(argv[1:])
    return args


def main(args):
    if args.mode == "0":
        eval_trained_model(args.config, args.ckpt_dir, args.out_dir,
                           args.images_glob,
                           helpers.parse_tfds_arguments(args),
                           args.lpips_weight_path)
    else:
        eval_trained_model_dist(args.out_dir + 'patches_real/', args.out_dir + 'patches_fake/', os.path.join(args.out_dir, "results.txt"))


if __name__ == '__main__':
    main(parse_args(sys.argv))

