import os
import numpy



def main():
    #'python -m hific.train_v2_pro --config hific --ckpt_dir ckpts/test_pro_4 --init_autoencoder_from_ckpt_dir ../ckpts_final/mse_lpips --num_steps 1M --latent_chanels 3072 --tfds_dataset_name coco2014'
    latent_channels_candidates= [4096] 
    #for channels in latent_channels_candidates:
        #hific.train('hific', 'ckpts/pro_' + str(channels), 20000 , './ckpts_final/mse_lpips', 8 ,  256, '', channels, True, tfds_arguments )
        
        #os.system('CUDA_VISIBLE_DEVICES=1 python -m hific.train_v2_pro --config hific --ckpt_dir ckpts/pro_' + str(channels) +' --init_autoencoder_from_ckpt_dir ../ckpts_final/mse_lpips --num_steps 20k --latent_chanels '+ str(channels) +' --tfds_dataset_name coco2014 < training.txt')
    latent_channels_candidates= [512, 2048, 3072, 4096] 
    for channels in latent_channels_candidates:
        os.system('CUDA_VISIBLE_DEVICES=1 python -m hific.evaluate --config hific --ckpt_dir ckpts/pro_' + str(channels) +' --out_dir out_'+str(channels) + ' --images_glob=../DIV2K_valid_HR/*.png --mode 0 --lpips_weight_path lpips_weight__net-lin_alex_v0.1.pb < evalutions.txt' )



if __name__ == '__main__':
  main()